from .views import (
    show_list,
    todo_list_detail,
    create_todo_list,
    edit_todo_list,
    todo_list_delete,
    todo_item_create,
    todo_item_update,
)
from django.urls import path

urlpatterns = [
    path("", show_list, name="show_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("templates/", create_todo_list, name="create_todo_list"),
    path("<int:id>/edit/", edit_todo_list, name="edit_todo_list"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
